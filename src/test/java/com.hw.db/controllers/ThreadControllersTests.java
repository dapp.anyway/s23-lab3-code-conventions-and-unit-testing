package com.hw.db.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.mockito.internal.verification.Times;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

class ThreadControllerTests {
    private Thread myThread;

    @BeforeEach
    @DisplayName("Create Thread")
    void createThread() {
        myThread = new Thread("thread", new Timestamp(0), "frm", "msg", "slug", "topic", 20);
    }

    @Test
    @DisplayName("ID and Slug")
    void testIdAndSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt("101"))).thenReturn(myThread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(myThread);
            assertEquals(myThread, controller.CheckIdOrSlug("101"), "Success ID");
            assertEquals(myThread, controller.CheckIdOrSlug("slug"), "Success Slug");
        }
    }

    @Test
    @DisplayName("Create Post")
    void testCreatePost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> postsList = Collections.emptyList();
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(myThread);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postsList), controller.createPost("slug", postsList), "Success");
        }
    }

    @Test
    @DisplayName("Get Post")
    void testGetPost() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            List<Post> postsList = Collections.emptyList();
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getPosts(myThread.getId(), 30, 2, null, false)).thenReturn(postsList);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(myThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postsList), controller.Posts("slug", 30, 2, null, false), "Success");
        }
    }

    @Test
    @DisplayName("Change Thread")
    void testChangeThread() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            Thread changeThread = new Thread("person", new Timestamp(2), "frm", "msg", "slug", "topic", 2030);
            changeThread.setId(2);
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("change")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(myThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(myThread), controller.change("change", myThread), "Success");
        }
    }

    @Test
    @DisplayName("Create vote")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User user = new User("person", "p.person@innopolis.ru", "Person Person", "Person");
                Vote vote = new Vote("person", 2);
                ThreadController controller = new ThreadController();
                threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(myThread);
                userMock.when(() -> UserDAO.Info("person")).thenReturn(user);
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(myThread), controller.createVote("slug", vote), "Success");
            }
        }
    }

    @Test
    @DisplayName("Thread information")
    void testThreadInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            ThreadController controller = new ThreadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(myThread);
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(myThread), controller.info("slug"), "Success");
        }
    }
}
